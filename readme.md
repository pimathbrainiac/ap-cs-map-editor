# AP CS Map Editor

This is a Map Editor program I made for the "AP CS Project" in 2015. The code is untouched since then, but I updated the NetBeans project and the map file itself.

##Controls
* **Arrow Keys** - Move the cursor to the desired location on the map.
* **Page Up/Page Down** - Select the tile to be placed.
* **Enter** - Place the tile selected at the location of the cursor.
* **Escape** - Leave the editor and save the map.

## Authors
* **Philip Nickerson** - *Lead Programmer* - [PhiNick](https://phinick.net)
* **Brady Bucknell** - *Game Designer, Secondary Programmer*
* **Everest Britt** - *Asset Designer*