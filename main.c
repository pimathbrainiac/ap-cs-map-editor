#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>

//Screen dimension constants
const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;
const int TILE_WIDTH = 32;
const int TILE_HEIGHT = 32;
//number of sprites
const int NUM_SPRITES = 16;

//The current tile to place
int currentTile = 0;

SDL_Rect cursorRect;

//map data
int map[24][32] = {
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11},
	{11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11, 11}};

//Loads map
int load_map(char*);

//Reads the map data and stores in map
void read_map();

//Writes the map data from map
void write_map();

//Loads media
int load_media();

//adds a tile at the current cursor location
void add_tile_at_cursor();

//Renders the screen
void render();

//Starts up SDL and creates window
int init();

//Frees media and shuts down SDL
void close();

//CSV file for the map
FILE *mapCSV;

//The window we'll be rendering to
SDL_Window *gWindow = NULL;

//The surface contained by the window
SDL_Surface *gScreenSurface = NULL;

//The cursor
SDL_Surface *gCursor = NULL;

//The spritesheet
SDL_Surface *gSprites = NULL;

int main(int argc, char **argv)
{
	if(!init())
	{
		exit(EXIT_FAILURE);
	}
	if(!load_media())
	{
		exit(EXIT_FAILURE);
	}
	SDL_UpdateWindowSurface(gWindow);
	int quit = 0;
	SDL_Event e;
	render();
	while(!quit)
	{
		while(SDL_PollEvent(&e) != 0)
		{
			if(e.type == SDL_QUIT)
			{
				quit = 1;
			}
		}
		const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
		if(currentKeyStates[SDL_SCANCODE_ESCAPE])
		{
			quit = 1;
		}
		if(currentKeyStates[SDL_SCANCODE_UP] && cursorRect.y > 0)
		{
			cursorRect.y-=32;
			render();
			SDL_Delay(200);
		}
		if(currentKeyStates[SDL_SCANCODE_DOWN] && cursorRect.y < SCREEN_HEIGHT - 32)
		{
			cursorRect.y+=32;
			render();
			SDL_Delay(200);
		}
		if(currentKeyStates[SDL_SCANCODE_LEFT] && cursorRect.x > 0)
		{
			cursorRect.x-=32;
			render();
			SDL_Delay(200);
		}
		if(currentKeyStates[SDL_SCANCODE_RIGHT] && cursorRect.x < SCREEN_WIDTH - 32)
		{
			cursorRect.x+=32;
			render();
			SDL_Delay(200);
		}
		if(currentKeyStates[SDL_SCANCODE_RETURN])
		{
			add_tile_at_cursor();
		}
		if(currentKeyStates[SDL_SCANCODE_PAGEUP] && currentTile < NUM_SPRITES - 1)
		{
			currentTile++;
			render();
			SDL_Delay(200);
		}
		if(currentKeyStates[SDL_SCANCODE_PAGEDOWN] && currentTile > 0)
		{
			currentTile--;
			render();
			SDL_Delay(200);
		}
	}
	close();
	return EXIT_SUCCESS;
}

int load_map(char *type)
{
	mapCSV = fopen("map.csv", type);
	return mapCSV;
}

void read_map()
{
	int row = 0;
	int col = 0;
	for (row = 0; row < 24; row++)
	{
		for(col = 0; col < 32; col++)
		{
			fscanf(mapCSV, "%d, ", &map[row][col]);
		}
	}
}

void write_map()
{
	int row = 0;
	int col = 0;
	for (row = 0; row < 24; row++)
	{
		for (col = 0; col < 32; col++)
		{
			fprintf(mapCSV, "%d,", map[row][col]);
		}
		fprintf(mapCSV, "\n");
	}
}

int load_media()
{
	int success = 1;
	gCursor = SDL_LoadBMP("cursor.bmp");
	if(gCursor == NULL)
	{
		success = 0;
	}
	gSprites = SDL_LoadBMP("sprites.bmp");
	if(gSprites == NULL)
	{
		success = 0;
	}
	cursorRect.x = 0;
	cursorRect.y = 0;
	cursorRect.w = TILE_WIDTH;
	cursorRect.h = TILE_HEIGHT;
	SDL_SetColorKey(gCursor, SDL_TRUE, 0x00FFFF);
	SDL_SetColorKey(gSprites, SDL_TRUE, 0x00FFFF);
	return success;
}

void add_tile_at_cursor()
{
	map[cursorRect.y / 32][cursorRect.x / 32] = currentTile;
	render();
}

void render()
{
	SDL_FillRect(gScreenSurface, NULL, 0x000000);
	int row = 0;
	int col = 0;
	SDL_Rect sprite;
	sprite.x = 0;
	sprite.y = 0;
	sprite.w = TILE_WIDTH;
	sprite.h = TILE_HEIGHT;
	SDL_Rect tile;
	tile.x = 0;
	tile.y = 0;
	tile.w = TILE_WIDTH;
	tile.h = TILE_HEIGHT;
	for(row = 0; row < 24; row++)
	{
		for(col = 0; col < 32; col++)
		{
			sprite.x = map[row][col] * 32;
			tile.x = col * 32;
			tile.y = row * 32;
			SDL_BlitSurface(gSprites, &sprite, gScreenSurface, &tile);
		}
	}
	sprite.x = currentTile * 32;
	SDL_BlitSurface(gSprites, &sprite, gScreenSurface, &cursorRect);
	SDL_BlitSurface(gCursor, NULL, gScreenSurface, &cursorRect);
	SDL_UpdateWindowSurface(gWindow);
}

int init()
{
	int success = 1;
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL failed to init video!");
		success = 0;
	}
	else
	{
		gWindow = SDL_CreateWindow("maped", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			printf("SDL failed to create Window!");
			success = 0;
		}
		else
		{
			gScreenSurface = SDL_GetWindowSurface(gWindow);
			if (!load_map("r"))
			{
				fclose(mapCSV);
				load_map("w");
				write_map();
				fclose(mapCSV);
				if (!load_map("r"))
				{
					printf("Failed to load/create the map CSV file");
					success = 0;
				}
			}
			else
			{
				read_map();
				fclose(mapCSV);
			}
		}
	}
	return success;
}

void close()
{
	//save map
	load_map("w");
	write_map();
	
	//Deallocate surface
	SDL_FreeSurface(gCursor);
	gCursor = NULL;
	
	//Destroy window
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	//Quit SDL subsystems
	SDL_Quit();
}